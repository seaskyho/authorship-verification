# Authorship Verification

### Introduction

This is my thesis for my master degree research project in University of Melbourne. 

I worked on the topic of Authorship Verification. Authorship veriﬁcation is a text-categorization task. Given a set of documents written by one author and a questioned document of unknown authorship, an authorship veriﬁcation task is to answer whether this questioned document is written by the same author or not. In this research project, I examined proﬁle-based character n-gram method’s performance on PAN13 and PAN15 authorship veriﬁcation corpora.

Due to confidentiality reasons, I'm not allowed share the code. Therefore, in this repository, there is only one pdf file, which is my thesis. Welcome to read it : )